from . import views

from django.conf.urls import url

urlpatterns = [
    url(r'^api/dsr/$', views.dsr, name='dsr'),
    url(r'^api/currency/$', views.currency, name='currency'),
    url(r'^api/territory/$', views.territory, name='territory'),
    url(r'^api/periodicity/$', views.periodicity, name='periodicity'),
    url(r'^api/statistics/$', views.statistics, name='statistics'),
]
