from django.core.paginator import Paginator
from django.db.models import Q

from rest_framework.decorators import api_view
from rest_framework.response import Response

from api import models, serializers


@api_view(['GET'])
def dsr(request):
    q = Q()
    request.GET = request.GET.copy()
    if request.GET.get('date_start'):
        q.add(Q(date_start=request.GET.pop('date_start')[0]), Q.AND)

    if request.GET.get('date_end'):
        q.add(Q(date_end=request.GET.pop('date_end')[0]), Q.AND)

    paginator = Paginator(
        models.DSR.objects.filter(
            q, **{k + '__in': v.split(',') for k,v in request.GET.items()}), 10
    )
    page = request.GET.get('page', 1)
    paginated_data = paginator.get_page(page)
    serialized_data = serializers.DSRSerializer(paginated_data, many=True).data
    return Response({
        'data': serialized_data,
        'current': paginated_data.number,
        'next': paginated_data.has_next()
    })


@api_view(['GET'])
def statistics(request):
    filtered_statistics = models.Statistics.objects.filter(
        dsr__in=map(int, request.GET['dsrs'].split(','))
    )
    data = serializers.StatisticsSerializer(filtered_statistics, many=True).data
    return Response({'data': data})


@api_view(['GET'])
def periodicity(request):
    data = serializers.PeriodicitySerializer(models.Periodicity.objects.all(), many=True).data
    return Response({'data': data})


@api_view(['GET'])
def territory(request):
    data = serializers.TerritorySerializer(models.Territory.objects.all(), many=True).data
    return Response({'data': data})


@api_view(['GET'])
def currency(request):
    data = serializers.CurrencySerializer(models.Currency.objects.all(), many=True).data
    return Response({'data': data})
