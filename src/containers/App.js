import React, { Component } from 'react';
import Form from './Form';
import Table from '../components/Table';
const Currency = require('../models/Currency');
const Territory = require('../models/Territory');
const Periodicity = require('../models/Periodicity');
const DSR = require('../models/DSR');
const Statistics = require('../models/Statistics');

class App extends Component {
  constructor() {
    super();

    this.state = {
      territories: [],
      currentTerritories: [],
      currencies: [],
      currentCurrencies: [],
      periodicities: [],
      currentPeriodicities: [],
      dsrs: [],
      currentStatuses: [],
      dateStart: null,
      dateEnd: null,
    };

    this.setDateStart = this.setDateStart.bind(this);
    this.setDateEnd = this.setDateEnd.bind(this);
    this.setTerritory = this.setTerritory.bind(this);
    this.setCurrency = this.setCurrency.bind(this);
    this.setStatus = this.setStatus.bind(this);
    this.setPeriodicity = this.setPeriodicity.bind(this);
  }

  buildQueryFilters = () => {
    let queryString = '';
    if (this.state.dateStart) {
      queryString = queryString + 'date_start=' + this.state.dateStart.format('YYYY-MM-DD') + '&';
    }

    if (this.state.dateEnd) {
      queryString = queryString + 'date_end=' + this.state.dateEnd.format('YYYY-MM-DD') + '&';
    }

    if (this.state.currentTerritories.length) {
      queryString = queryString + 'territory=' + this.state.currentTerritories.map(e => e.value.id) + '&';
    }

    if (this.state.currentCurrencies.length) {
      queryString = queryString + 'currency=' + this.state.currentCurrencies.map(e => e.value.id) + '&';
    }

    if (this.state.currentPeriodicities.length) {
      queryString = queryString + 'periodicity=' + this.state.currentPeriodicities.map(e => e.value.id) + '&';
    }

    if (this.state.currentStatuses.length) {
      queryString = queryString + 'status=' + this.state.currentStatuses.map(e => e.value);
    }

    return queryString;
  }

  fetchDSR = () => {
    fetch('http://localhost:8000/api/dsr/?' + this.buildQueryFilters())
      .then(response => {
        response.json().then(data => {
          this.setState({dsrs: data.data.map(e => new DSR(e))}, () => {
            if (this.state.dsrs.length) {
              fetch(
                'http://localhost:8000/api/statistics/?dsrs=' + this.state.dsrs.map((dsr) => dsr.id)
              )
                .then(response => {
                  response.json().then(data => {
                    for(let s of data.data) {
                      s = new Statistics(s);
                      const item = this.state.dsrs.find(x => x.id === s.id);
                      item.statistics = s;
                      const filtered = this.state.dsrs.filter(x => x.id !== s.id);
                      this.setState({dsrs: [...filtered, item]});
                    }
                  });
                });
            }
          });
        });
      });
  }

  componentDidMount() {
    fetch('http://localhost:8000/api/currency/')
      .then(response => {
        response.json().then(data => {
          this.setState({currencies: data.data.map(e => new Currency(e))});
        });
      });

    fetch('http://localhost:8000/api/territory/')
      .then(response => {
        response.json().then(data => {
          this.setState({territories: data.data.map(e => new Territory(e))});
        });
      });

    fetch('http://localhost:8000/api/periodicity/')
      .then(response => {
        response.json().then(data => {
          this.setState({periodicities: data.data.map(e => new Periodicity(e))});
        });
      });

    this.fetchDSR();
  }

  getSelectedValues = (filtered, option) => {
    if (option.selected) {
      filtered.push(option.value);
    }
    return filtered;
  }

  setDateStart = (date) => {
    this.setState({dateStart: date},
                  () => this.fetchDSR());
  }

  setDateEnd = (date) => {
    this.setState({dateEnd: date},
                  () => this.fetchDSR());
  }

  setStatus = (selected) => {
    this.setState({currentStatuses: selected},
                  () => this.fetchDSR());
  }

  setTerritory = (selected) => {
    this.setState({currentTerritories: selected},
                   () => this.fetchDSR());
  }

  setCurrency = (selected) => {
    this.setState({currentCurrencies: selected},
                  () => this.fetchDSR());
  }

  setPeriodicity = (selected) => {
    this.setState({currentPeriodicities: selected},
                  () => this.fetchDSR());
  }

  render() {
    return (
        <div style={{display: 'flex', flexFlow: 'row wrap'}}>
          <div style={{flex: 1, flexBasis: '100%'}}>
            <Form
              setDateStart={this.setDateStart}
              setDateEnd={this.setDateEnd}
              setStatus={this.setStatus}
              setTerritory={this.setTerritory}
              setCurrency={this.setCurrency}
              setPeriodicity={this.setPeriodicity}
              territories={this.state.territories}
              currentTerritories={this.currentTerritories}
              statuses={this.state.statuses}
              currentStatuses={this.currentStatuses}
              currencies={this.state.currencies}
              currentCurrencies={this.currentCurrencies}
              periodicities={this.state.periodicities}
              currentPeriodicities={this.state.currentPeriodicities}
              dateStart={this.state.dateStart}
              dateEnd={this.state.dateEnd}
            />
          </div>
          <div style={{flex: 1, flexBasis: '100%'}}>
            <div style={{display: 'flex', flexFlow: 'row wrap', alignItems: 'flex-start'}}>
              <Table dsrs={this.state.dsrs} />
            </div>
          </div>
        </div>
    );
  }
}

export default App;
