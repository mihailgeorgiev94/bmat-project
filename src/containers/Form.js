import React, { Component } from 'react';
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../css/form.css';

class Form extends Component {
  constructor(props) {
    super(props);

    this.statuses = [
      'upcoming',
      'failed',
      'completed',
    ];
  }

  render() {
    const selectStyles = {
      container: (base) => ({
        ...base,
        margin: '0 5px 0 5px',
      }),
    };
    return (
        <form style={{display: 'flex', flexFlow: 'row wrap'}}>
          <div className='options-container'>
            <label>Start Date</label>
            <DatePicker
              selected={this.props.dateStart}
              onChange={this.props.setDateStart}
            />
          </div>
          <div className='options-container'>
            <label>End Date</label>
              <DatePicker
                selected={this.props.dateEnd}
                onChange={this.props.setDateEnd}
              />
          </div>
          <div className='options-container'>
            <label>Status</label>
            <Select
              value={this.props.currentStatuses}
              onChange={this.props.setStatus}
              options={this.statuses.map(v => {return {value: v, label: v};})}
              isSearchable={{isSearchable: true}}
              isClearable={{isClearable: true}}
              isMulti
              styles={selectStyles}
            />
          </div>
          <div className='options-container'>
            <label>Territory</label>
              <Select
                value={this.props.currentTerritories}
                onChange={this.props.setTerritory}
                options={this.props.territories.map(v => {return {value: v, label: v.name};})}
                isSearchable={{isSearchable: true}}
                isClearable={{isClearable: true}}
                isMulti
                styles={selectStyles}
              />
          </div>
          <div className='options-container'>
            <label>Currency</label>
            <Select
              value={this.props.currentCurrencies}
              onChange={this.props.setCurrency}
              options={this.props.currencies.map(v => {return {value: v, label: v.name};})}
              isSearchable={{isSearchable: true}}
              isClearable={{isClearable: true}}
              isMulti
              styles={selectStyles}
            />
          </div>
          <div className='options-container'>
            <label>Periodicity</label>
            <Select
              value={this.props.currentPeriodicities}
              onChange={this.props.setPeriodicity}
              options={this.props.periodicities.map(v => {return {value: v, label: v.name};})}
              isSearchable={{isSearchable: true}}
              isClearable={{isClearable: true}}
              isMulti
              styles={selectStyles}
            />
          </div>
        </form>
    );
  }
}

export default Form;
