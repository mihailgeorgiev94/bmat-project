class Currency {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.symbol = data.symbol;
    this.code = data.code;
  }
}

module.exports = Currency;
