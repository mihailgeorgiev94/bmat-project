class Statistics {
  constructor(data) {
    this.id = data.id;
    this.countSales = data.count_sales;
    this.dsr = data.dsr;
    this.freeUnits = data.free_units;
    this.netRevenue = data.net_revenue;
    this.release = data.release;
    this.releaseTransactions = data.release_transactions;
    this.resources = data.resources;
  }
}

module.exports = Statistics;
