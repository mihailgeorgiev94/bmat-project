class DSR {
  constructor(data) {
    this.id = data.id;
    this.currency = data.currency;
    this.dateEnd = data.date_end;
    this.dateStart = data.date_start;
    this.path = data.path;
    this.periodicity = data.periodicity;
    this.status = data.status;
    this.territory = data.territory;
    this.statistics = {};
  }
}

module.exports = DSR;
