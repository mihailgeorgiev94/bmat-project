class Territory {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.code2 = data.code_2;
    this.code3 = data.code_3;
  }
}

module.exports = Territory;
