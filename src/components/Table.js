import React from 'react';
import Card from './Card';

const Table = (props) => {
  return (
    props.dsrs.map((e) => {
      return (
          <div style={{flex: 1, flexBasis: '100%'}}>
            <Card
              key={e.id}
              dsr={e}
            />
          </div>
      );
    })
  );
};

export default Table;
