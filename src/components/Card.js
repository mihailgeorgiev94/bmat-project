import React from 'react';
import '../css/card.css';

const Card = (props) => {
  return (
     <div className='card'>
       <section className='card-item'>
         <div style={{display: 'flex'}} className='card-heading'>
           <div>
             <label>Beginning Date:</label><br/>
               <p>{props.dsr.dateStart}</p>
           </div>
           <div>
             <label>Ending Date:</label><br/>
             <p>{props.dsr.dateEnd}</p>
           </div>
           <div>
             <label>Status:</label><br/>
             <p>{props.dsr.status}</p>
           </div>
         </div>
         <div style={{display: 'flex', flexFlow: 'row wrap'}} className='container'>
           <div>
             <label>Number of Sales:</label><br/>
             <p>{props.dsr.statistics.countSales}</p>
           </div>
           <div>
             <label>Number of Free Units:</label><br/>
             <p>{props.dsr.statistics.freeUnits}</p>
           </div>
           <div>
             <label>Net Revenue:</label><br/>
             <p>{props.dsr.statistics.netRevenue}</p>
           </div>
           <div>
             <label>Release:</label><br/>
             <p>{props.dsr.statistics.release}</p>
           </div>
           <div>
             <label>Release Transactions:</label><br/>
             <p>{props.dsr.statistics.releaseTransactions}</p>
           </div>
           <div>
             <label>Resources:</label><br/>
             <p>{props.dsr.statistics.resources}</p>
           </div>
         </div>
       </section>
     </div>
  );
};

export default Card;
